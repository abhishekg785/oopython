from sys import argv

script,filename = argv

def print_all(f):
    print (f.read())
    
def rewind(f):
    f.seek(0)

def print_a_line(line_count,f):
    print (line_count,f.readline())
    
current_file = open(filename)

print ("first let us print the whole file")

print_all(current_file)

rewind(current_file)

# you might be thinking that how values changed if passed  by value,
# well in python everything is an object so the changed occurs in the object

print ("let's print all the three lines")

current_line = 1
print_a_line(current_line,current_file)
current_line = current_line + 1
print_a_line(current_line,current_file)
current_line = current_line + 1
print_a_line(current_line,current_file) 

current_file.seek(0)

current_file.close()
