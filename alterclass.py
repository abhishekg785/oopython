class Parent(object):
    def alter(self):
        print ("parent altered")
    
class Child(Parent):
    def alter(self):
        print ("calling alter of the parent first")
        super(Child,self).alter()
        print ("maybe alter of parent called")
        
father  = Parent()
son = Child()

father.alter()
son.alter()
