from sys import argv
from os.path import exists  #for file pathnames 

script,source_file,dest_file = argv

print ("the result for files whether exists or not")
print ("for sourcefile %s result is: %r" % (source_file,exists(source_file)))
print ("for destination file %s result is: %r" % (dest_file,exists(dest_file)))

#now copying the contents of file to another

target = open(source_file)
content = target.read()

source = open(dest_file,'w')
source.write(content)

print ("ok then copying of file done")

target.close()
source.close()


