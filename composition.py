class Other(object):
    def override(self):
        print ("other override")
        
    def implicit(self):
        print ("other implicit")
     
    def alter(self):
        print ("other alter")
        
class Child(object):
    def __init__(self):
        self.other = Other()
        
    def implicit(self):
        self.other.implicit()
       
    def override(self):
        self.other.override()
       
    def alter(self):
        self.other.alter()
        
        
son = Child()
son.alter()
