class Parent(object):
    def __init__(self):
        print ("this is the constructor of the parent class")
    
    def display(self):
        print ("you are in the display function of the parent")
       
class Child(Parent):
    def __init__(self):
        super(Child,self).__init__()
        print ("you are in the constructor of the child class")
        
    def display(self):
        print ("you are in the display function of the child class")
        
        
son = Child()
son.display()   
