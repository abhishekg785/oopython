class Song(object):
	
	def __init__(self,lyrics):
		self.lyrics = lyrics

	def sing_me_a_song(self):
		for line in self.lyrics:
			print (line)

#instantiate objects
happy_bday_song = Song(["love is forever and you are my dream come true"]);

owl_city = Song(["its  dreamy island and nnowhere comes here"]);

happy_bday_song.sing_me_a_song()
owl_city.sing_me_a_song()
