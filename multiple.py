class Parent1(object):
    def __init__(self):
        print ("this is the cons of the parent1 class")
       
    def display(self):
        print ("this is the display function of the PARENT1 class")
        
class Parent2(object):
    def __init__(self):
        print ("this is the cons of the PARENT2 class")     
       
    def display(self):
        print ("this is the display function of the PARENT2 class") 
        
        
#this is the chid class and inherits multiple classes       
class child(Parent1,Parent2):
    def __init__(self): 
        super(child,self).__init__()
        print ("this is the cons of the child class")
        
    def call2(self):
        super(child,self).display()
 
 
c = child()
c.call2()
